import { suite, test, only, retries } from "mocha-typescript";
import { IUserRepo } from "interfaces/repos";
import UserRepo from "../../components/repos/user_repo";
import UserManager from "../../components/managers/user_manager";
import { IUser } from "interfaces/descriptors";
import { deepStrictEqual, strictEqual } from "assert";
import { IUserManager } from "interfaces/managers";

@suite
class UserManagerTest {
    private userRepo: IUserRepo;
    private userManager: IUserManager;
    private user1: IUser = {
        username: "1",
        name: "1",
        email: "1@gmail.com",
        password: "11111"
    };

    private user2: IUser = {
        username: "2",
        name: "2",
        email: "2@gmail.com",
        password: "11111"
    };

    private user3: IUser = {
        username: "3",
        name: "3",
        email: "3@gmail.com",
        password: "11111"
    };

    private user4: IUser = {
        username: "4",
        name: "4",
        email: "4@gmail.com",
        password: "11111"
    };

    private user5: IUser = {
        username: "5",
        name: "5",
        email: "5@gmail.com",
        password: "11111"
    };

    constructor() {
        this.userRepo = new UserRepo();
        this.userManager = new UserManager(this.userRepo);
    }

    after() {
        this.userRepo.clear();
    }

    @test
    "should be able to register"() {
        const user = this.userManager.register(this.user1);
        deepStrictEqual(user, this.user1);
    }

    @test
    "should throw error when registering user without username, email, or password"() {
        try {
            this.userManager.register({
                email: "e@gmail.com",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                email: "e@gmail.com"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }
    }

    @test
    "should throw error when registering user if another user with the same username exists"() {
        this.userManager.register(this.user1)
        try {
            this.userManager.register(this.user1)
        } catch (error) {
            strictEqual(error.message, `User with username \"${this.user1.username}\" already exists.`)
        }
    }

    @test
    "should be able to read user"() {
        this.userManager.register(this.user1)
        deepStrictEqual(this.user1, this.userManager.readUser(this.user1.username))
    }

    @test
    "should throw error when reading user if user not found"() {
        try {
            this.userManager.readUser("wrong")
        } catch (error) {
            strictEqual(error.message, `User with username \"wrong\" not found`)
        }
        // throws(() => this.userManager.readUser("wrong"), Error, `User with username \'wrong\' not found`)
    }

    @test
    "should be able to delete user"() {
        this.userManager.register(this.user1)
        strictEqual(true, this.userManager.deleteUser(this.user1.username))
    }

    @test
    "should throw error when deleting user if user not found"() {
        try {
            this.userManager.deleteUser("wrong")
        } catch (error) {
            strictEqual(error.message, `User with username \"wrong\" not found`)
        }
        // throws(() => this.userManager.deleteUser("wrong"), Error, `User with username \'wrong\' not found`)
    }

    @test
    "should be able to update user"() {
        this.userManager.register(this.user1);
        const updatedUser = this.userManager.updateUser(this.user1.username, {
            name: "11"
        } as any);
        const user = { ...this.user1 };
        user.name = "11";
        deepStrictEqual(updatedUser, user);
    }

    @test
    "should throw error when updating user if user not found"() {
        const { password, ...rest } = this.user1
        try {
            this.userManager.updateUser("wrong", rest)
        } catch (error) {
            strictEqual(error.message, `User with username \"wrong\" not found`)
        }
        // throws(() => this.userManager.updateUser("wrong", rest), Error, `User with username \'wrong\' not found`)
    }

    @test
    "should throw error when updating user if user object contains password"() {
        this.userManager.register(this.user1)
        try {
            this.userManager.updateUser(this.user1.username, this.user1)
        } catch (error) {
            strictEqual(error.message, `Password is not allowed in this method`)
        }
        // throws(() => this.userManager.updateUser(this.user1.username, this.user1), Error, `Password is not allowed in this method`)
    }

    @test
    "should be able to update user's password"() {
        this.userManager.register(this.user1)
        const updatedUser = this.userManager.updatePassword(this.user1.username, this.user1.password, "newPassword")
        const cloneUser = this.user1
        cloneUser.password = "newPassword"
        deepStrictEqual(cloneUser, updatedUser)
    }

    @test
    "should throw error when updating user's password if user not found"() {
        try {
            this.userManager.updatePassword("wrongUsername", this.user1.password, "newPassword")
        } catch (error) {
            strictEqual(error.message, `User with username \"wrongUsername\" not found`)
        }
    }

    @test
    "should throw error when updating user's password if old password is not the same as current password"() {
        this.userManager.register(this.user1)
        try {
            this.userManager.updatePassword(this.user1.username, "wrongOldPassword", "newPassword")
        } catch (error) {
            strictEqual(error.message, "Invalid old password")
        }
    }

    @test
    "should be able to login"() {
        this.userManager.register(this.user1)
        strictEqual(true, this.userManager.login(this.user1.username, this.user1.password))
    }

    @test
    "should throw error when login if password is wrong"() {
        this.userManager.register(this.user1)
        try {
            this.userManager.login(this.user1.username, "wrongPassword")
        } catch (error) {
            strictEqual(error.message, "Invalid username or password")
        }
    }
    @test
    "should throw error when login if user does not exist"() {
        this.userManager.register(this.user1)
        try {
            this.userManager.login("wrongUsername", this.user1.password)
        } catch (error) {
            strictEqual(error.message, "Invalid username or password")
        }
    }

    @test
    "should be able to list user"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.listUser({});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, {
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user1, this.user2, this.user3, this.user4, this.user5]
        });
    }

    @test
    "should be able to filter user when listing user"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.listUser({username: this.user1.username})
        strictEqual(1, users.data.length)
        deepStrictEqual({
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user1]
        }, users)
    }

    @test
    "should be able to sort user when listing user"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);
        
        const users = this.userManager.listUser({}, {sort: {name: "asc"}})
        strictEqual(5, users.data.length)
        deepStrictEqual({
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user1, this.user2, this.user3, this.user4, this.user5]
        }, users)
    }

    @test
    "should be able to paginate user when listing user"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.listUser({}, {page: 1, limit: 3})
        strictEqual(3, users.data.length)
        deepStrictEqual({
            page: 1,
            limit: 3,
            total: 5,
            data: [this.user1, this.user2, this.user3]
        }, users)
    }

    @test
    "should be able to list users in descending order"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.descendingUsername()
        strictEqual(5, users.data.length)
        deepStrictEqual({
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user5, this.user4, this.user3, this.user2, this.user1]
        }, users)
    }
}
