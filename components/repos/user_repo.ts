import { IUserRepo } from 'interfaces/repos';
import { IUser } from 'interfaces/descriptors';
import * as _ from 'lodash';
import { Component } from 'merapi';

export default class UserRepo extends Component implements IUserRepo {
    constructor() {
        super();
    }

    private users: { [username: string]: IUser } = {};

    public list(
        query: Object
    ): (options?: {
        page?: number;
        limit?: number;
        sort?: { [column: string]: 'asc' | 'desc' };
    }) => IUser[] {
        const { username, email, password, name } = query as Partial<IUser>;
        // const { page, limit, sort } = options
        let res: IUser[] = [];
        if (username) {
            if (this.users[username]) {
                res = [this.users[username]];
            }
        } else {
            res = Object.keys(this.users).map(k => this.users[k])
            res = res.filter(user => {
                if (email) return user.email === email;
                return true
            });
            res = res.filter(user => {
                if (password) return user.password === password;
                return true
            });
            res = res.filter(user => {
                if (name) return user.name === name;
                return true
            });
        }
        return options => {
            if (options){
                const { limit, sort } = options;
                let { page } = options;
                if (!page) page = 1
                if (limit) {
                    res = res.slice(page - 1, page + limit - 1);
                } else {
                    res = res.slice(page - 1, 10);
                }
                if (sort) {
                    return _.orderBy(
                        res,
                        Object.keys(sort),
                        Object.keys(sort).map(key => sort[key])
                    );
                }
            } 
            return res
            
        };
    }

    public count(query: Object): number {
        return this.list(query)({}).length;
    }

    public create(object: IUser): IUser {
        const { username } = object;

        if (this.users[username]) {
            return null;
        }

        this.users[username] = object;
        return object;
    }

    public read(username: string): IUser {
        const user = this.users[username];
        return user ? user : null
    }

    public update(username: string, object: Partial<IUser>): IUser {
        const target = this.users[username];
        if (target) {
            this.users[username] = {
                ...target,
                ...object
            };
            return this.users[username];
        }
        return null
    }

    public delete(username: string): boolean {
        if (this.users[username]) {
            this.users = _.omit(this.users, [username])
            return true
        };
        return false
    }

    public clear(): void {
        this.users = {};
    }
}
