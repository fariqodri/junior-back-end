import { IUserManager } from "interfaces/managers";
import { Component } from "merapi";
import { IUserRepo, Paginated } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";

export default class UserManager extends Component implements IUserManager {
    constructor(private userRepo: IUserRepo) {
        super();
    }

    public listUser(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): Paginated<IUser> {
        const usersWithQuery = this.userRepo.list(query)
        let result: Paginated<IUser> = {
            page: 1,
            data: usersWithQuery({page: 1, limit: 10, ...options}),
            total: this.userRepo.list({})().length,
            limit: 10
        }
        if (options) {
            const { page, limit, sort } = options
            result = {
                page: page ? page : 1,
                data: usersWithQuery({page: 1, limit: 10, ...options}),
                limit: limit ? limit : 10,
                total: this.userRepo.list({})().length
            }
        }
        return result
    }

    public descendingUsername(): Paginated<IUser> {
        return this.listUser({}, { sort: { username: "desc" } });
    }

    public readUser(username: string): IUser {
        return this.userRepo.read(username)
    }

    public deleteUser(username: string): boolean {
        return this.userRepo.delete(username)
    }

    public register(object: IUser): IUser {
        const { username, email, password } = object;

        if (!username || !email || !password) {
            throw new Error("Username, email, and password are required.");
        }

        const user = this.userRepo.create(object);
        if (!user) {
            throw new Error(`User with username \"${username}\" already exists.`);
        }
        return user;
    }

    public login(username: string, password: string): boolean {
        const userWithUsername = this.readUser(username)
        if (userWithUsername) {
            return userWithUsername.password === password
        }
        return false
    }

    public updateUser(username: string, object: Partial<IUser>): IUser {
        const updated = this.userRepo.update(username, object)
        if (updated === null) {
            throw Error(`User with username \"${username}\" not found`)
        }
        return updated
    }

    public updatePassword(
        username: string,
        oldPassword: string,
        newPassword: string
    ): IUser {
        const user = this.userRepo.read(username)
        if (user) {
            if (user.password !== oldPassword) {
                throw Error("Invalid old password")
            }
            return this.userRepo.update(username, {password: newPassword})
        }
        throw Error(`User with username \"${username}\" not found`)
    }
}
